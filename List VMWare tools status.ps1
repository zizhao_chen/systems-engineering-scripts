Connect-VIServer –Server 0.0.0.0  –User administrator –Password password
$csvFileName="{0}\VM-{1:yyyyMMdd-HHmmdd}.csv" -f $Env:TEMP,(get-date)
get-vm | % { get-view $_.id } | select Name, @{ Name="ToolsVersion"; Expression={$_.config.tools.toolsVersion}},@{ Name="ToolStatus"; Expression={$_.Guest.ToolsVersionStatus}}|export-csv  -NoTypeInformation $csvFileName
ii $csvFileName